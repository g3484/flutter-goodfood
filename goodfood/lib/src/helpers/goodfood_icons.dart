import 'package:flutter/widgets.dart';

/// Logo personnalisés spécifiques au projet
///
/// Pour ajouter un nouveau logo, aller sur le site https://fluttericon.com/
/// Les différentes étapes sont expliquées ici : https://dev.to/techwithsam/how-to-add-your-own-custom-icons-in-your-flutter-application-made-easy-1bnj
class GoodfoodIcons {
  GoodfoodIcons._();

  static const _kFormFam = 'MyFlutterApp';
  static const String? _kFontPkg = null;

  /// Logo Apple
  static const IconData apple = IconData(0xf179,
    fontFamily: _kFormFam,
    fontPackage: _kFontPkg
  );

  /// Logo gitHub
  static const IconData gitHub = IconData(0xf09b,
    fontFamily: _kFormFam,
    fontPackage: _kFontPkg
  );

  /// Logo google
  static const IconData google = IconData(0xf1a0,
    fontFamily: _kFormFam,
    fontPackage: _kFontPkg
  );
}