import 'package:flutter/material.dart';
import 'package:goodfood/config/environment.dart';

/// Barre de navigation
/// Returns AppBar
AppBar appBar({Widget? title}) {
  return AppBar(
    backgroundColor: Colors.transparent,
    iconTheme: IconThemeData(color: Environment.getColorTheme()),
    elevation: 0.0,
    title: title
  );
}

/// Input forms decoration
/// String [hint] label
/// Color [errorColor] error color
/// Returns InputDecoration
InputDecoration getInputDecoration(
    {required String hint, required Color errorColor}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
    filled: true,
    fillColor: Color(0xffE5E5E5),
    hintText: hint,
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12.0),
        borderSide: BorderSide(color: Environment.getColorTheme(), width: 2.0)),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: errorColor),
      borderRadius: BorderRadius.circular(12.0),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: errorColor),
      borderRadius: BorderRadius.circular(12.0),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey.shade200),
      borderRadius: BorderRadius.circular(12.0),
    ),
  );
}

/// Séparateur avec du text au milieu
/// Returns Widget
Widget dividerWithText(String text) {
  return Padding(
    padding: EdgeInsets.only(left: 10.0, right: 10.0),
    child: Row(
      children: <Widget>[
        Expanded(
          child: Divider(
            color: Color(0xff061737),
            height: 0.5,
            thickness: 2
          )
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          child: Text("$text"),
        ),
        Expanded(
          child: Divider(
            color: Color(0xff061737),
            height: 0.5,
            thickness: 2
          )
        ),
      ]
    ),
  );
}

/// Boutons d'authentification avec les réseaux sociaux
/// Returns ElevatedButton
ElevatedButton socialNetworkButton(IconData icon) {
  return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.only(top: 12, bottom: 12),
        shape: CircleBorder(
          side: BorderSide(
            color: Colors.grey,
          ),
        ),
        primary: Color(0xffE5E5E5),
      ),
      child: Icon(icon, color: Color(0xff061737)),
      onPressed: () {
      }
  );
}