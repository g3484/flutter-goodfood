class Order {
  final String id;
  final String userId;
  final String userName;
  final String address;
  final List<dynamic> products;
  final double price;
  final String dateOfOrder;
  final String version;
  final String status;

  Order._({
    required this.id,
    required this.userId,
    required this.userName,
    required this.address,
    required this.products,
    required this.price,
    required this.dateOfOrder,
    required this.version,
    required this.status,
  });

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order._(
      id: json['id'],
      userId: json['userId'],
      userName: json['userName'],
      address: json['address'],
      products: json['products'],
      price: double.parse(json['price'].toString()),
      dateOfOrder: json['dateOfOrder'],
      version: json['version'].toString(),
      status: json['status']
    );
  }
}