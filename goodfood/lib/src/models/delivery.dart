class Delivery {
  final String id;
  final String userName;
  final String address;
  final String status;
  final String dateOfOrder;
  final int version;

  Delivery._({
    required this.id,
    required this.userName,
    required this.address,
    required this.status,
    required this.dateOfOrder,
    required this.version
  });

  factory Delivery.fromJson(Map<String, dynamic> json) {
    return Delivery._(
        id: json['id'],
        userName: json['userName'],
        address: json['address'],
        status: json['status'],
        dateOfOrder: json['dateOfOrder'],
        version: json['version']
    );
  }
}