class Category {
  final String id;
  final String name;

  Category._({ required this.id, required this.name });

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category._(id: json['id'], name: json['name']);
  }
}