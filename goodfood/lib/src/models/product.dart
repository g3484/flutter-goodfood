import 'dart:convert';

class Product {
  final String id;
  final String name;
  final String description;
  final String imageUrl;
  final String publicIdImage;
  final String price;
  final String version;
  final String idCategory;
  int quantity;

  Product._({
    required this.id,
    required this.name,
    required this.description,
    required this.imageUrl,
    required this.publicIdImage,
    required this.price,
    required this.version,
    required this.idCategory,
    required this.quantity
  });

  Map<String, dynamic> toJson() => {
    'id': this.id,
    'name': this.name,
    'description': this.description,
    'imageUrl': this.imageUrl,
    'publicIdImage': this.publicIdImage,
    'price': this.price,
    'version': this.version,
    'idCategory': this.idCategory,
    'quantity': 1
  };

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product._(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      imageUrl: json['imageUrl'],
      publicIdImage: json['publicIdImage'],
      price: json['price'].toString(),
      version: json['version'].toString(),
      idCategory: json['idCategory'],
      quantity: 1
    );
  }
}