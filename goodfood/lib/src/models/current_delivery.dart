import 'dart:convert';

import 'package:goodfood/src/models/deliverer_coordinates.dart';

class CurrentDelivery {
  final String id;
  final String delivererId;
  final DelivererCoordinates delivererCoordinates;
  final String orderId;
  final String clientName;
  final String clientAddress;
  final String status;
  final int version;

  CurrentDelivery._({
    required this.id,
    required this.delivererId,
    required this.delivererCoordinates,
    required this.orderId,
    required this.clientName,
    required this.clientAddress,
    required this.status,
    required this.version
  });

  factory CurrentDelivery.fromJson(Map<String, dynamic> json) {
    return CurrentDelivery._(
      id: json['id'],
      delivererId: json['delivererId'],
      delivererCoordinates: DelivererCoordinates.fromJson(json['delivererCoordinates']),
      orderId: json['orderId'],
      clientName: json['clientName'],
      clientAddress: json['clientAddress'],
      status: json['status'],
      version: json['version']
    );
  }
}