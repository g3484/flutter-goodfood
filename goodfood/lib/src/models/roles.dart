/// Model Role
/// Les différents rôles sont encapsulés dans des constantes
class Role {
  /// Rôle client
  static const String CLIENT = 'ROLE_CLIENT';

  /// Rôle livreur
  static const String DELIVERER = 'ROLE_DELIVERER';

  /// Rôle administrateur
  static const String ADMIN = 'ROLE_ADMIN';
}