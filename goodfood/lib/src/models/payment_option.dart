class PaymentOption {
  final int id;
  final String name;
  final String image;

  PaymentOption._({ required this.id, required this.name, required this.image });

  factory PaymentOption.fromJson(Map<String, dynamic> json) {
    return PaymentOption._(
        id: json['id'],
        name: json['name'],
        image: json['image']
    );
  }
}