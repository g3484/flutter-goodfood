class Address {
  final int id;
  final String userId;
  final String lane;
  final String zipCode;
  final String city;
  final bool? mainAddress;

  Address._({
    required this.id,
    required this.userId,
    required this.lane,
    required this.zipCode,
    required this.city,
    this.mainAddress
  });

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address._(
      id: json['id'],
      userId: json['userId'].trim(),
      lane: json['lane'].trim(),
      zipCode: json['zipCode'].trim(),
      city: json['city'].trim(),
      mainAddress: json['default']
    );
  }
}