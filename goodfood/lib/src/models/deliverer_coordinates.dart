class DelivererCoordinates {
  final double latitude;
  final double longitude;

  DelivererCoordinates._({ required this.latitude, required this.longitude});

  factory DelivererCoordinates.fromJson(Map<String, dynamic> json) {
    return DelivererCoordinates._(
      latitude: json['latitude'],
      longitude: json['longitude']
    );
  }
}