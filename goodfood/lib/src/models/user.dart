class User {
  final String id;
  final String firstname;
  final String lastname;
  final String mailAddress;
  final String phone;
  final String password;
  final String? avatar;

  User._({
    required this.id,
    required this.firstname,
    required this.lastname,
    required this.mailAddress,
    required this.phone,
    required this.password,
    this.avatar
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return new User._(
      id: json['id'],
      firstname: json['firstname'],
      lastname: json['lastname'],
      mailAddress: json['mailAddress'],
      password: json['password'],
      phone: json['phone'],
      avatar: json['avatar']
    );
  }
}