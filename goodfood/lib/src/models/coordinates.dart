class Coordinates {
  final double longitude;
  final double latitude;

  Coordinates._({ required this.longitude, required this.latitude });

  factory Coordinates.fromJson(Map<String, dynamic> json) {
    return Coordinates._(longitude: json['longitude'], latitude: json['latitude']);
  }
}