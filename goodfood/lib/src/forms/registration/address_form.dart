import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/helpers/goodfood_icons.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/services/auth/auth.dart';
import 'package:goodfood/views/login/login.dart';

class AddressForm extends StatefulWidget {
  final String firstname;
  final String lastname;
  final String mailAddress;
  final String phone;
  final String password;

  AddressForm({
    required this.firstname,
    required this.lastname,
    required this.mailAddress,
    required this.phone,
    required this.password
  });

  @override
  _AddressFormState createState() => _AddressFormState();
}

class _AddressFormState extends State<AddressForm> {
  AuthService _authService = new AuthService();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _addressController = new TextEditingController();
  final _cityController = new TextEditingController();
  final _zipCodeController = new TextEditingController();

  bool isLoading = false;

  @override
  void dispose() {
    _addressController.dispose();
    _cityController.dispose();
    _zipCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: isLoading == false
        ? Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  right: 16.0, left: 16.0, bottom: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Bonjour,',
                    style: TextStyle(
                        color: Color(0xff061737),
                        fontSize: 50.0,
                        fontWeight: FontWeight.w900),
                    textAlign: TextAlign.start,
                  ),
                  Text("Présente toi !",
                    style: TextStyle(
                        fontSize: 22.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 32.0, right: 24.0, left: 24.0),
              child: TextFormField(
                  controller: _addressController,
                  textAlignVertical: TextAlignVertical.center,
                  textInputAction: TextInputAction.next,
                  validator: (v) =>
                  v == null || v.isEmpty
                      ? "Tu dois renseigner ton adresse de domicile"
                      : null,
                  style: const TextStyle(fontSize: 18.0),
                  cursorColor: Environment.getColorTheme(),
                  decoration: getInputDecoration(
                      hint: 'Adresse',
                      errorColor: Theme
                          .of(context)
                          .errorColor)),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 32.0, right: 24.0, left: 24.0),
              child: TextFormField(
                  controller: _zipCodeController,
                  textAlignVertical: TextAlignVertical.center,
                  textInputAction: TextInputAction.next,
                  validator: (v) =>
                  v == null || v.isEmpty
                      ? "Tu dois renseigner ton code postal"
                      : null,
                  style: const TextStyle(fontSize: 18.0),
                  cursorColor: Environment.getColorTheme(),
                  decoration: getInputDecoration(
                      hint: 'Code postal',
                      errorColor: Theme
                          .of(context)
                          .errorColor)),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 32.0, right: 24.0, left: 24.0),
              child: TextFormField(
                  controller: _cityController,
                  textAlignVertical: TextAlignVertical.center,
                  textInputAction: TextInputAction.next,
                  validator: (v) =>
                  v == null || v.isEmpty
                      ? "Tu dois renseigner ta ville"
                      : null,
                  style: const TextStyle(fontSize: 18.0),
                  cursorColor: Environment.getColorTheme(),
                  decoration: getInputDecoration(
                      hint: 'Ville',
                      errorColor: Theme
                          .of(context)
                          .errorColor)),
            ),
            Padding(
              padding: const EdgeInsets.all(40.0),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      side: BorderSide(
                        color: Color(0xff061737),
                      ),
                    ),
                    backgroundColor: Color(0xff061737),
                  ),
                  child: Text("S'inscrire"),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      setState(() {
                        isLoading = true;
                      });
                      _authService.registration(
                          widget.firstname,
                          widget.lastname,
                          widget.mailAddress,
                          widget.phone,
                          widget.password,
                          _addressController.text,
                          _cityController.text,
                          _zipCodeController.text
                      ).then((response) {
                        setState(() {
                          isLoading = false;
                        });
                        switch (response.statusCode) {
                          case 201:
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Ton compte a bien été créé ! Tu vas recevoir un mail de confirmation"),
                                  backgroundColor: Colors.green,
                                )
                            );
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => LoginPage()));
                            break;
                          case 400:
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("${jsonDecode(response.body)['message']}"),
                                  backgroundColor: Colors.orange,
                                )
                            );
                            break;
                          case 500:
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Une erreur est survenue"),
                                  backgroundColor: Colors.red,
                                )
                            );
                            break;
                        }
                      });
                    }
                  }
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 25),
              child: dividerWithText("Ou continuer avec"),
            ),
            Padding(
              padding: EdgeInsets.only(top: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  socialNetworkButton(GoodfoodIcons.apple),
                  socialNetworkButton(GoodfoodIcons.google),
                  socialNetworkButton(GoodfoodIcons.gitHub),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 25, bottom: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(right: 10.0),
                      child: Text("Vous avez déjà un compte ?")),
                  InkWell(
                    child: Text("Connexion",
                        style: TextStyle(
                            color: Color(0xff8190F5),
                            fontWeight: FontWeight.bold
                        )
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ) : Center(child: CircularProgressIndicator()),
    );
  }
}