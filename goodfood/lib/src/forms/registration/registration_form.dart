import 'package:flutter/material.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/forms/registration/address_form.dart';
import 'package:goodfood/src/helpers/goodfood_icons.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/views/login/login.dart';

class RegistrationForm extends StatefulWidget {
  @override
  _RegistrationFormState createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _firstnameController = new TextEditingController();
  final _lastnameController = new TextEditingController();
  final _mailController = new TextEditingController();
  final _phoneController = new TextEditingController();
  final _passwordController = new TextEditingController();

  bool isLoading = false;

  @override
  void dispose() {
    _firstnameController.dispose();
    _lastnameController.dispose();
    _mailController.dispose();
    _phoneController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading == false
        ? Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                right: 16.0, left: 16.0, bottom: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Bonjour,',
                  style: TextStyle(
                      color: Color(0xff061737),
                      fontSize: 50.0,
                      fontWeight: FontWeight.w900),
                  textAlign: TextAlign.start,
                ),
                Text("Présente toi !",
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.grey,
                    fontWeight: FontWeight.w500
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _firstnameController,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois renseigner ton prénom" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Prénom',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _lastnameController,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois renseigner ton nom" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Nom',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _mailController,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois renseigner ton adresse e-mail" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Adresse e-mail',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _phoneController,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois renseigner ton numéro de téléphone" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Numéro de téléphone',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _passwordController,
                obscureText: true,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois définir un mot de passe" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Mot de passe',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(top: 12, bottom: 12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    side: BorderSide(
                      color: Color(0xff061737),
                    ),
                  ),
                  primary: Color(0xff061737),
                ),
                child: Text("Suivant"),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AddressForm(
                          firstname: _firstnameController.text,
                          lastname: _lastnameController.text,
                          mailAddress: _mailController.text,
                          phone: _phoneController.text,
                          password: _passwordController.text,
                        )));
                  }
                }
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25),
            child: dividerWithText("Ou continuer avec"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                socialNetworkButton(GoodfoodIcons.apple),
                socialNetworkButton(GoodfoodIcons.google),
                socialNetworkButton(GoodfoodIcons.gitHub),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25, bottom: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(right: 10.0), child: Text("Vous avez déjà un compte ?")),
                InkWell(
                  child: Text("Connexion",
                      style: TextStyle(
                          color: Color(0xff8190F5),
                          fontWeight: FontWeight.bold
                      )
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    ): Center(child: CircularProgressIndicator());
  }
}