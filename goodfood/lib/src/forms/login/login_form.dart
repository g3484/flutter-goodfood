import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/helpers/goodfood_icons.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/roles.dart';
import 'package:goodfood/src/services/auth/auth.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/views/dashboard/dashboard_builder.dart';
import 'package:goodfood/views/deliverer/orders/deliverer_orders.dart';
import 'package:goodfood/views/home/homepage.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  AuthService _authService = new AuthService();
  Storage _storage = new Storage();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _mailController = new TextEditingController();
  final _passwordController = new TextEditingController();

  bool isLoading = false;

  @override
  void dispose() {
    _mailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading == false
        ? Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                right: 16.0, left: 16.0, bottom: 30.0),
            child: Text(
              'Connexion',
              style: TextStyle(
                  color: Color(0xff061737),
                  fontSize: 50.0,
                  fontWeight: FontWeight.w900),
              textAlign: TextAlign.start,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _mailController,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois renseigner ton adresse e-mail" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Adresse e-mail',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 32.0, right: 24.0, left: 24.0),
            child: TextFormField(
                controller: _passwordController,
                obscureText: true,
                textAlignVertical: TextAlignVertical.center,
                textInputAction: TextInputAction.next,
                validator: (v) => v == null || v.isEmpty ? "Tu dois renseigner ton mot de passe" : null,
                style: const TextStyle(fontSize: 18.0),
                cursorColor: Environment.getColorTheme(),
                decoration: getInputDecoration(
                    hint: 'Mot de passe',
                    errorColor: Theme.of(context).errorColor)),
          ),
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.only(top: 12, bottom: 12),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  side: BorderSide(
                    color: Color(0xff061737),
                  ),
                ),
                backgroundColor: Color(0xff061737),
              ),
              child: Text("Connexion"),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _authService.login(_mailController.text, _passwordController.text).then((response) {
                    var json = jsonDecode(response.body);
                    switch (response.statusCode) {
                      case 200:
                        _storage.setJwt(json['jwt']);
                        _storage.setRefreshToken(json['refreshToken']);
                        _storage.setUserId(json['user']['id']);
                        _storage.setUsername(json['user']['mailAddress']);

                        _authService.hasRole(json['jwt'], Role.DELIVERER).then((hasRole) {
                          if (hasRole) {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (context) =>
                                    DelivererOrders()
                                ), (Route<dynamic> route) => false
                            );
                          } else {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (context) =>
                                    Dashboard()
                                ), (Route<dynamic> route) => false
                            );
                          }
                        });
                        break;
                      case 401:
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Adresse e-mail ou mot de passe incorrect"),
                              backgroundColor: Colors.red,
                            )
                        );
                        break;
                      case 500:
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Une erreur est survenue"),
                              backgroundColor: Colors.red,
                            )
                        );
                        break;
                    }
                  });
                }
              }
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25),
            child: dividerWithText("Ou continuer avec"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                socialNetworkButton(GoodfoodIcons.apple),
                socialNetworkButton(GoodfoodIcons.google),
                socialNetworkButton(GoodfoodIcons.gitHub),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(right: 10.0), child: Text("Vous n'avez pas de compte ?")),
                InkWell(
                  child: Text("Inscription",
                      style: TextStyle(
                        color: Color(0xff8190F5),
                        fontWeight: FontWeight.bold
                      )
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Homepage()));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    ): Center(child: CircularProgressIndicator());
  }
}