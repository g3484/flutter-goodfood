/// Extension StringExtension
/// Encapsule les extensions sur les types string
extension StringExtension on String {
  /// Permet de mettre le premier caractère d'une chaîne en majuscule
  /// Ex: "hello word" = "Hello world"
  /// [Returns] String
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
  }
}