import 'dart:async';
import 'dart:convert';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:http/http.dart';

/// JwtService
/// Encapsulation des méthodes concernant le JWT
class JwtService {
  Storage _storage = new Storage();

  /// Rafraichissement du jwt
  /// [Returns] Réponse HTTP
  Future<Response> refreshToken() async {
    Uri url = Uri.parse(Environment.get("USER_API") + "/api/users/refresh_token");
    String token = await _storage.getJwt;
    String username = await _storage.getUsername;

    var body = jsonEncode({
      "token": token,
      "username": username
    });

    final response = await post(url,
        headers: {
          "Content-Type": "application/json"
        },
        body: body
    );

    return response;
  }
}