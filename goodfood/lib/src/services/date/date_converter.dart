import 'package:intl/intl.dart';

/// Class DateConverter
/// Permet de convertir les dates
class DateConverter {
  /// Permet de convertir une date type UTC en string
  /// Ex : Convertit yyyy-MM-ddTHH:mm:ss en dd/MM/yyyy
  /// [Returns] date
  static String toFrenchFormatFromUTCType(String date) {
    DateTime dateToParse = new DateFormat("yyyy-MM-ddTHH:mm:ss").parse(date);
    String formattedDate = DateFormat("dd/MM/yyyy").format(dateToParse);
    return formattedDate;
  }
}