/// Class OrderStatus
/// Service encapsulant les méthodes concernant les statuts des commandes
class OrderStatus {
  /// Traduction des statuts en français
  /// [param] String status
  /// [Returns] Statut en français
  static String translateStatus(String status) {
    String translation = '';

    switch (status) {
      case 'CREATED':
        translation = 'créée';
        break;
      case 'CANCELLED':
        translation = 'annulée';
        break;
      case 'AWAITING_PAYMENT':
        translation = 'en attente de paiement';
        break;
      case 'AWAITING_DELIVERY':
        translation = 'en attente de livraison';
        break;
      case 'IN_PROGRESS':
        translation = 'en cours de livraison';
        break;
      case 'COMPLETED':
        translation = 'terminée';
        break;
      default:
        throw new Exception("Le statut n'existe pas.");
    }
    return translation;
  }
}