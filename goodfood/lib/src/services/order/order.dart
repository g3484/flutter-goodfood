import 'dart:convert';

import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/models/order.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:http/http.dart';

/// Service Order
/// Encapsulation des méthodes concernant les commandes
class OrderService {
  Storage _storage = new Storage();

  /// Récupération des commandes
  /// [Returns] liste des commandes
  Future<List<Order>> getOrders() async {
    Uri url = Uri.parse(Environment.get("ORDERS_API"));
    String token = await _storage.getJwt;

    final response = await get(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );

    List<dynamic> json = jsonDecode(response.body);
    List<Order> orders = List<Order>.from(json.map((e) => Order.fromJson(e)).toList());

    return orders;
  }

  /// Ajout d'une commande dans le panier
  /// [String] username
  /// [String] address
  /// [List] productIds
  /// [Returns] Response
  Future<Response> addOrder(String username, String address, List<String> productIds) async {
    Uri url = Uri.parse(Environment.get("ORDERS_API"));
    String token = await _storage.getJwt;

    var body = jsonEncode({
      "userName": username,
      "address": address,
      "productIds": productIds
    });

    final response = await post(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
      body: body
    );
    return response;
  }

  /// Suppression d'une commande.
  /// [Param] String id de la commande
  /// [Returns] HTTP Response
  Future<Response> removeOrder(String id) async {
    Uri url = Uri.parse(Environment.get("ORDERS_API") + id);
    String token = await _storage.getJwt;

    final response = await delete(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );

    return response;
  }
}