import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/views/home/homepage.dart';
import 'package:http/http.dart';
import 'package:jwt_decode/jwt_decode.dart';

/// AuthService
/// Contient les méthodes relatives aux comptes utilisateurs
class AuthService {
  final _storage = FlutterSecureStorage();

  /// Création du compte utilisateur
  /// Returns Réponse HTTP
  Future<Response> registration(
      String firstname,
      String lastname,
      String mailAddress,
      String phone,
      String password,
      String address,
      String city,
      String zipCode
    ) async {
    Uri url = Uri.parse(Environment.get("USER_API") + "registration");

    var body = jsonEncode({
      "user": {
        "firstname": firstname,
        "lastname": lastname,
        "mailaddress": mailAddress,
        "phone": phone,
        "password": password
      },
      "address": {
        "lane": address,
        "city": city,
        "zipcode": zipCode
      }
    });

    final response = await post(url,
      headers: {
        "Content-Type": "application/json"
      },
      body: body
    );

    return response;
  }

  /// Connexion à son compte utilisateur
  /// [String] mail adresse e-mail
  /// [String] password mot de passe
  /// [Returns] Réponse http
  Future<Response> login(String mail, String password) async {
    Uri url = Uri.parse(Environment.get("USER_API") + "login");

    var body = jsonEncode({
      "mailaddress": mail,
      "password": password
    });

    final response = await post(url,
        headers: {
          "Content-Type": "application/json"
        },
        body: body
    );
    return response;
  }

  /// Vérification du rôle de l'utilisateur
  /// Le rôle est extrait du JWT
  /// Si l'utilisateur a le rôle spécifié en paramètre,
  ///   la méthode renvoi "true", sinon "false
  /// [String] jwt
  /// [String] rôle souhaité
  /// [Returns] bool
  Future<bool> hasRole(String jwt, String role) async {
    bool hasRole = false;

    var parsedToken = Jwt.parseJwt(jwt);
    Map<String, dynamic> json = parsedToken;
    List<String> roles = json['roles'].toString().split(',');

    roles.forEach((item) {
      if (item.trim() == role) {
        hasRole = true;
      }
    });
    return hasRole;
  }

  /// Déconnexion
  /// Suppression de toutes les variables en mémoire
  /// [BuildContext] context
  /// [Returns] void
  Future<void> logout(BuildContext context) async {
    await _storage.deleteAll();
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
        Homepage()), (Route<dynamic> route) => false);
  }
}