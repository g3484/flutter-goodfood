import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/coordinates.dart';
import 'package:goodfood/src/services/user/user.dart';
import 'package:http/http.dart';

/// Class City Service
/// Service encapsulant les méthodes concernant les adresses
class CityService {
  final _storage = FlutterSecureStorage();

  /// Récupération des coordonées des villes
  /// Ceci concerne les adresses de domicile des utilisateurs
  /// Les coordonnées sont récupérées grâce à l'API du gouvernement
  /// [Return] Coordinates
  Future<Coordinates> getCoordinates() async {
    UserService _userService = new UserService();
    String? userId = await _storage.read(key: "userId");
    List<Address> addresses = await _userService.findAddresses(userId!);
    Address address = addresses.firstWhere((x) => x.mainAddress == true);

    String q = address.lane.replaceAll(" ", "+") + "+" + address.zipCode + "+" + address.city;
    Uri url = Uri.parse(Environment.get("ADRESSES_GOUV_API") + "/search/?q=$q");

    final response = await get(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        }
    );

    var jsonResponse = jsonDecode(response.body)['features'][0]['geometry']['coordinates'];
    var json = jsonEncode({
      "latitude": jsonResponse[1],
      "longitude": jsonResponse[0]
    });

    Coordinates coordinates = Coordinates.fromJson(jsonDecode(json));
    return coordinates;
  }

  /// Récupération des coordonnées à partir de l'adresse passée en paramètres
  /// [String] adresse
  /// [Returns] coordonnées
  Future<Coordinates> getCoordinatesByAddress(String address) async {
    Uri url = Uri.parse(Environment.get("ADRESSES_GOUV_API") + "/search/?q=$address");

    final response = await get(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        }
    );

    var jsonResponse = jsonDecode(response.body)['features'][0]['geometry']['coordinates'];
    var json = jsonEncode({
      "latitude": jsonResponse[1],
      "longitude": jsonResponse[0]
    });

    Coordinates coordinates = Coordinates.fromJson(jsonDecode(json));
    return coordinates;
  }
}