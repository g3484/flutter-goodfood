import 'dart:async';
import 'dart:convert';

import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/user.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:http/http.dart';

/// UserService
/// Service contenant les méthodes concernant l'utilisateur
class UserService {
  Storage _storage = new Storage();

  /// Récupération d'un utilisateur par son Id
  /// [String] id
  /// [Returns] User
  Future<User> findById(String id) async {
    Uri url = Uri.parse(Environment.get("USER_API") + "profile/$id");
    String token = await _storage.getJwt;

    final response = await get(url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer: $token"
        },
    );

    User user = User.fromJson(jsonDecode(response.body)['user']);
    return user;
  }

  /// Récupération d'un utilisateur par son adresse e-mail
  /// [String] mailAddress
  /// [Returns] User
  Future<User> findByMailAddress(String mailAddress) async {
    Uri url = Uri.parse(Environment.get("USER_API") + "search/by-mail/$mailAddress");
    String token = await _storage.getJwt;

    final response = await get(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );

    return User.fromJson(jsonDecode(response.body));
  }

  /// Récupération des adresses d'un utilisateur
  /// [String] id de l'utilisateur
  /// [Returns] list des adresses
  Future<List<Address>> findAddresses(String id) async {
    Uri url = Uri.parse(Environment.get("USER_API") + "address/all/$id");
    String token = await _storage.getJwt;

    final response = await get(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );
    List<dynamic> json = jsonDecode(response.body);
    List<Address> addresses = List<Address>.from(json.map((e) => Address.fromJson(e)).toList());

    return addresses;
  }
}