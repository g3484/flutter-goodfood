import 'dart:convert';

import 'package:goodfood/src/models/payment_option.dart';

class PaymentService {
  Future<List<PaymentOption>> getPaymentOptions() async {
    String jsonString = '[{"id": 1, "name": "Carte de crédit", "image": "assets/images/credit-card.png"}, {"id": 2, "name": "Stripe", "image": "assets/images/Stripe-logo.png"}, {"id": 3, "name": "Apple Pay", "image": "assets/images/2560px-Apple_Pay_logo.svg.png"}, {"id": 4, "name": "Coinbase", "image": "assets/images/coinbase.d491999a12d7c093ec6cd83ad64a1e23.png"}]';

    List<dynamic> json = jsonDecode(jsonString);
    List<PaymentOption> paymentOptions = List<PaymentOption>
        .from(json.map((e) => PaymentOption.fromJson(e)).toList());

    return paymentOptions;
  }
}