import 'dart:convert';
import 'dart:io';

import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/models/current_delivery.dart';
import 'package:goodfood/src/models/delivery.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:http/http.dart';
import 'package:path/path.dart';

/// Service Delivery
/// Encapsulation des méthodes concernant les livraisons
class DeliveryService {
  Storage _storage = new Storage();

  /// Récupération des livraisons en attente de livreur
  /// [Returns] Liste des commandes
  Future<List<Delivery>> getAwaitDeliveries() async {
    Uri url = Uri.parse(Environment.get("DELIVERY_API"));
    String token = await _storage.getJwt;

    final response = await get(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );

    List<dynamic> json = jsonDecode(response.body);
    List<Delivery> deliveries = List<Delivery>.from(json.map((e) => Delivery.fromJson(e)).toList());

    return deliveries;
  }

  /// Récupération des livraison effectuées
  /// [Returns] Livraisons
  Future<List<CurrentDelivery>> getCurrentDeliveries() async {
    Uri url = Uri.parse(Environment.get("DELIVERY_API") + "/current");
    String token = await _storage.getJwt;

    final response = await get(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );

    List<dynamic> json = jsonDecode(response.body);
    List<CurrentDelivery> deliveries = List<CurrentDelivery>.from(json.map((e) => CurrentDelivery.fromJson(e)).toList());

    return deliveries;
  }

  /// Récupération d'une commmande
  /// [Param] ID de la commande (Order ID)
  /// [Returns] Delivery
  Future<Delivery> getDelivery(String orderId) async {
    Uri url = Uri.parse(Environment.get("DELIVERY_API") + "/details/$orderId");
    String token = await _storage.getJwt;

    final response = await get(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
    );

    return Delivery.fromJson(jsonDecode(response.body));
  }

  /// Création d'une nouvelle livraison
  /// [String] orderId Id de la commande
  /// [double] latitude du livreur
  /// [double] longitude du livreur
  /// [Returns] HttpResponse
  Future<Response> createDelivery(String orderId, double? latitude, double? longitude) async {
    Uri url = Uri.parse(Environment.get("DELIVERY_API"));
    String token = await _storage.getJwt;

    var body = jsonEncode({
      'orderId': orderId,
      'delivererCoordinates': {
        'latitude': latitude,
        'longitude': longitude
      }
    });

    final response = await post(url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer: $token"
      },
      body: body
    );

    return response;
  }

  /// Mise à jour des coordonnées GPS du livreur
  /// Cette méthode est appelée toutes les 30 secondes
  /// [String] id
  /// [double] latitude
  /// [double] longitude
  /// [Returns] HttpResponse
  Future<Response> updateDelivererLocation(String id, double? latitude, double? longitude) async {
    Uri url = Uri.parse(Environment.get("DELIVERY_API") + '/$id');
    String token = await _storage.getJwt;

    var body = jsonEncode({
      'delivererCoordinates': {
        'latitude': latitude,
        'longitude': longitude
      }
    });

    final response = await patch(url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer: $token"
        },
        body: body
    );
    return response;
  }

  /// Validation d'une livraison
  /// [String] id de la livraison
  /// [File] image
  /// [Returns] Response
  Future<StreamedResponse> deliveryCompleted(String id, File file) async {
    String token = await _storage.getJwt;

    MultipartRequest request = MultipartRequest('POST', Uri.parse(Environment.get("DELIVERY_API") + '/$id'));
    request.files.add(
      MultipartFile(
        'image',
        File(file.path).readAsBytes().asStream(),
        File(file.path).lengthSync(),
        filename: basename(file.path)
      )
    );
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };
    request.headers.addAll(headers);

    StreamedResponse response = await request.send();
    return response;
  }

  /// Annuler une livraison
  /// [String] id de la livraison
  /// [Returns] response
  Future<Response> deliveryCancelled(String id) async {
    Uri url = Uri.parse(Environment.get("DELIVERY_API") + '/$id');
    String token = await _storage.getJwt;

    final response = await delete(url,
        headers: {
          "Authorization": "Bearer: $token"
        },
    );
    return response;
  }
}