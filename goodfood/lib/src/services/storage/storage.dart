import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// Class Storage
/// Encapsulation des méthodes utilisant le package FlutterSecureStorage
class Storage {
  final _storage = FlutterSecureStorage();

  /// Enregistrement du JWT lors de l'authentification de l'utilisateur
  /// [String] jwt
  /// [Returns] void
  Future<void> setJwt(String jwt) async {
    await _storage.write(key: "jwt", value: jwt.trim());
  }

  /// Récupération du Jwt
  /// [Returns] JWT
  Future<dynamic> get getJwt async {
    var jwt = await _storage.read(key: "jwt");
    return jwt == null ? "" : jwt as FutureOr<String>;
  }

  /// Enregistrement du token de rafraichissement lors de l'authentification de l'utilisateur
  /// [String] token
  /// [Returns] void
  Future<void> setRefreshToken(String token) async {
    await _storage.write(key: "refreshToken", value: token.trim());
  }

  /// Récupération du Refresh Token
  /// [Returns] Refresh token
  Future<dynamic> get getRefreshToken async {
    var token = await _storage.read(key: "refreshToken");
    return token as FutureOr<String>;
  }

  /// Enregistrement de l'id d'un utilisateur
  /// [String] id
  /// [Returns] void
  Future<void> setUserId(String id) async {
    await _storage.write(key: "userId", value: id.trim());
  }

  /// Récupération de l'id de l'utilisateur
  /// [Returns] id
  Future<dynamic> get getUserId async {
    var userId = await _storage.read(key: "userId");
    return userId as FutureOr<String>;
  }

  /// Enregistrement de l'adresse e-mail d'un utilisateur
  /// [String] username
  /// [Returns] void
  Future<void> setUsername(String username) async {
    await _storage.write(key: "username", value: username.trim());
  }

  /// Récupération de l'adresse e-mail d'un utilisateur
  /// [Returns] adresse e-mail
  Future<dynamic> get getUsername async {
    var username = await _storage.read(key: "username");
    return username as FutureOr<String>;
  }
}