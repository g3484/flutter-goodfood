import 'dart:convert';

import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/models/category.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Service Category
/// Encapsulation des méthodes concernant l'API products
class ProductService {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  /// Récupération de toutes les catégories
  /// [Returns] Categories
  Future<List<Category>> getCategories() async {
    Uri url = Uri.parse(Environment.get("PRODUCTS_API") + "category");

    final response = await get(url,
      headers: {
        "Content-Type": "application/json"
      },
    );

    List<dynamic> json = jsonDecode(response.body);
    List<Category> categories = List<Category>.from(json.map((e) => Category.fromJson(e)).toList());

    return categories;
  }

  /// Récupération de tous les produits
  /// [Returns] produits
  Future<List<Product>> getProducts() async {
    Uri url = Uri.parse(Environment.get("PRODUCTS_API"));

    final response = await get(url,
      headers: {
        "Content-Type": "application/json"
      },
    );

    List<dynamic> json = jsonDecode(response.body);
    List<Product> products = List<Product>.from(json.map((e) => Product.fromJson(e)).toList());

    return products;
  }

  /// Récupération d'un produit
  /// [Param] String id
  /// [Returns] Produit
  Future<Product> getProduct(String id) async {
    Uri url = Uri.parse(Environment.get("PRODUCTS_API") + id);

    final response = await get(url,
      headers: {
        "Content-Type": "application/json"
      },
    );

    Product product = Product.fromJson(jsonDecode(response.body));

    return product;
  }

  /// Ajout d'un produit dans le local storage
  /// [Param] List<Product> liste des produits
  /// [Returns] void
  Future<void> addToCart(List<Product> product) async {
    final SharedPreferences prefs = await _prefs;
    prefs.setString("cart", jsonEncode(product));
  }

  /// Récupération du contenu du panier
  /// [Returns] Liste des produits
  Future<List<Product>> getCartContent() async {
    final SharedPreferences prefs = await _prefs;

    List<dynamic> json = jsonDecode(prefs.getString("cart").toString());
    List<Product> products = List<Product>.from(json.map((e) => Product.fromJson(e)).toList());

    return products;
  }
}