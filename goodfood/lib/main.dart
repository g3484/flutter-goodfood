// @dart=2.12

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/services/jwt/jwt.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/views/dashboard/dashboard_builder.dart';
import 'package:goodfood/views/home/homepage.dart';
import 'package:responsive_framework/responsive_framework.dart';

Future main() async {
  HttpOverrides.global = MyHttpOverrides();
  await dotenv.load(fileName: ".env");
  runApp(Goodfood());
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

class Goodfood extends StatefulWidget {
  _GoodfoodState createState() => _GoodfoodState();
}

class _GoodfoodState extends State<Goodfood> {
  final _storage = new Storage();
  final _jwtService = new JwtService();

  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);
    return MaterialApp(
      title: Environment.get('APP_NAME'),
      theme: ThemeData(
        fontFamily: 'Roboto'
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate
      ],
      supportedLocales: [
        Locale('en'),
        Locale('fr')
      ],
      builder: (context, child) {
        child = ResponsiveWrapper.builder(
            child,
            maxWidth: 1200,
            minWidth: 480,
            defaultScale: true,
            breakpoints: [
              ResponsiveBreakpoint.resize(480, name: MOBILE),
              ResponsiveBreakpoint.autoScale(600, name: TABLET),
              ResponsiveBreakpoint.resize(1000, name: DESKTOP),
            ],
            background: Container(color: Color(0xFFF5F5F5)));
        child = MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child);
        return child;
      },
      home: FutureBuilder(
        future: Future.wait([
          _storage.getJwt,
          _storage.getRefreshToken
        ]),
        builder: (context, snapshot) {
          List tokens = snapshot.data as List;

          if (snapshot.hasData) {
            var jwt = tokens[0].split('.').toString();
            if (jwt.length != 3) {
              return Homepage();
            } else {
              var payload = jsonDecode(ascii.decode(base64.decode(base64.normalize(jwt[1]))));
              if (DateTime.fromMillisecondsSinceEpoch(payload["exp"]*1000).isAfter(DateTime.now())) {
                _jwtService.refreshToken();
              }
              return Dashboard();
            }
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}