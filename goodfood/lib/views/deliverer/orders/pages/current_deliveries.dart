import 'package:flutter/material.dart';
import 'package:goodfood/src/extensions/string_extension.dart';
import 'package:goodfood/src/models/current_delivery.dart';
import 'package:goodfood/src/services/delivery/delivery.dart';
import 'package:goodfood/src/services/order/order_status.dart';
import '../map/current/current_delivery_map.dart';
import '../map/current/current_delivery_map_builder.dart';
import 'package:goodfood/views/deliverer/orders/no_delivery_found.dart';

class CurrentDeliveries extends StatelessWidget {
  final _deliveryService = new DeliveryService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _deliveryService.getCurrentDeliveries(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error);
          }

          List<CurrentDelivery> deliveries = snapshot.data;

          return snapshot.hasData
              ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text("Mes livraisons en cours",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 28.0,
                    color: Color(0xff061737),
                  ),
                ),
              ),
              Flexible(
                child: deliveries.length > 0
                    ? ListView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    itemCount: deliveries.length,
                    itemBuilder: (BuildContext context, int index) {
                      CurrentDelivery delivery = deliveries[index];
                      return GestureDetector(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(15.0),
                                    child: Text("Commande #${delivery.id.substring(delivery.id.length - 5, delivery.id.length).toUpperCase()}",
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 15.0, bottom: 15.0),
                                    child: Text("${OrderStatus.translateStatus(delivery.status.toUpperCase()).capitalize()}",
                                      style: TextStyle(
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff0B0B4BE)
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>
                                  CurrentDeliveryMapBuilder(currentDelivery: delivery))
                          );
                        },
                      );
                    }
                ) : NoDeliveryFound(),
              ),
            ],
          ) : Center(child: CircularProgressIndicator());
        }
    );
  }
}