import 'dart:async';

import 'package:flutter/material.dart';
import 'package:goodfood/src/models/current_delivery.dart';
import 'package:goodfood/src/models/delivery.dart';
import 'package:goodfood/src/services/city/city.dart';
import 'package:goodfood/src/services/delivery/delivery.dart';
import 'current_delivery_map.dart';
import 'package:location/location.dart';

class CurrentDeliveryMapBuilder extends StatefulWidget {
  final CurrentDelivery currentDelivery;

  CurrentDeliveryMapBuilder({ required this.currentDelivery });

  @override
  _CurrentDeliveryMapBuilderState createState() => _CurrentDeliveryMapBuilderState();
}

class _CurrentDeliveryMapBuilderState extends State<CurrentDeliveryMapBuilder> {
  final _deliveryService = new DeliveryService();
  final _cityService = new CityService();
  final _location = new Location();

  Timer? timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(new Duration(seconds: 30), (timer) {
      _location.getLocation().then((location) {
        _deliveryService.updateDelivererLocation(widget.currentDelivery!.id, location.latitude, location.longitude).then((response) {
          print(response.statusCode);
        });
      });
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        _location.getLocation(),
        _cityService.getCoordinatesByAddress(widget.currentDelivery.clientAddress)
      ]),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
        }
        return snapshot.hasData
          ? CurrentDeliveryMap(
              location: snapshot.data[0],
              deliveryId: widget.currentDelivery!.id,
              clientName: widget.currentDelivery!.clientName,
              coordinates: snapshot.data[1],
            )
          : Center(child: CircularProgressIndicator());
      }
    );
  }
}