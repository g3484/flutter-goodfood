import 'dart:async';

import 'package:flutter/material.dart';
import 'package:goodfood/src/models/delivery.dart';
import 'package:goodfood/src/services/city/city.dart';
import 'package:goodfood/src/services/delivery/delivery.dart';
import 'awaiting_delivery_map.dart';
import 'package:location/location.dart';

class AwaitingDeliveryMapBuilder extends StatefulWidget {
  final Delivery delivery;
  final String deliveryId;

  AwaitingDeliveryMapBuilder({ required this.delivery, required this.deliveryId });

  @override
  _AwaitingDeliveryMapBuilderState createState() => _AwaitingDeliveryMapBuilderState();
}

class _AwaitingDeliveryMapBuilderState extends State<AwaitingDeliveryMapBuilder> {
  final _deliveryService = new DeliveryService();
  final _cityService = new CityService();
  final _location = new Location();

  Timer? timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(new Duration(seconds: 30), (timer) {
      _location.getLocation().then((location) {
        _deliveryService.updateDelivererLocation(widget.deliveryId, location.latitude, location.longitude).then((response) {
          print(response.statusCode);
        });
      });
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        _location.getLocation(),
        _cityService.getCoordinatesByAddress(widget.delivery.address)
      ]),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
        }
        return snapshot.hasData
          ? AwaitingDeliveryMap(
              location: snapshot.data[0],
              deliveryId: widget.deliveryId,
              clientName: widget.delivery.userName,
              coordinates: snapshot.data[1],
            )
          : Center(child: CircularProgressIndicator());
      }
    );
  }
}