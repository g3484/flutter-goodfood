import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:goodfood/src/models/coordinates.dart';
import 'package:goodfood/src/models/deliverer_coordinates.dart';
import 'package:goodfood/views/deliverer/delivery/delivery_completed.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';

class AwaitingDeliveryMap extends StatelessWidget {
  final LocationData location;
  final String deliveryId;
  final String clientName;
  final Coordinates coordinates;

  AwaitingDeliveryMap({
    required this.location,
    required this.deliveryId,
    required this.clientName,
    required this.coordinates
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          FlutterMap(
            options: MapOptions(
                center: LatLng(location.latitude!, location.longitude!),
                zoom: 13.0
            ),
            layers: [
              TileLayerOptions(
                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                subdomains: ['a', 'b', 'c'],
              ),
              MarkerLayerOptions(
                markers: <Marker>[
                  Marker(
                    width: 80.0,
                    height: 80.0,
                    point: LatLng(location.latitude!, location.longitude!),
                    builder: (ctx) =>
                        Icon(Icons.location_on, color: Colors.blueAccent)
                  ),
                  Marker(
                      width: 80.0,
                      height: 80.0,
                      point: LatLng(coordinates.latitude, coordinates.longitude),
                      builder: (ctx) =>
                          Icon(Icons.location_on, color: Colors.red)
                  )
                ]
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.4,
              child: Card(
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                shadowColor: Colors.grey,
                elevation: 5.0,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("5min - 3km",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff061737)
                        ),
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Flexible(
                            child: Text("${clientName.trim()}"),
                          ),
                          Icon(Icons.local_phone_rounded)
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Row(
                        children: <Widget>[
                          Text("Note : ",
                            style: TextStyle(
                              fontWeight: FontWeight.bold
                            ),
                          ),
                          Text("Livrer à la porte")
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 50, 20, 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              padding: const EdgeInsets.all(12),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                                side: BorderSide(
                                    color: Color(0xff0061737)
                                ),
                              ),
                              backgroundColor: Color(0xff0061737)
                          ),
                          child: Text("Livraison effectuée",
                            style: TextStyle(
                                fontSize: 16.0
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    DeliveryCompleted(deliveryId: deliveryId))
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            padding: const EdgeInsets.all(12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              side: BorderSide(color: Colors.white),
                            ),
                            backgroundColor: Colors.white
                          ),
                          child: Text("Annuler la livraison",
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.orange
                            ),
                          ),
                          onPressed: () {
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ),
        ],
      )
    );
  }
}