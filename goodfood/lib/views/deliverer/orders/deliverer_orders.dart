import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/views/deliverer/orders/pages/awaiting_deliveries.dart';
import 'package:goodfood/views/deliverer/orders/pages/current_deliveries.dart';
import 'package:goodfood/views/deliverer/widgets/slider_box.dart';
import 'package:goodfood/views/widgets/bottom_navigation_bar/deliverer/app_bottom_navigation_bar.dart';
import 'package:goodfood/views/widgets/bottom_navigation_bar/deliverer/bottom_navigation_bar_routes.dart';
import 'package:page_indicator/page_indicator.dart';

class DelivererOrders extends StatefulWidget {
  @override
  _DelivererOrdersState createState() => _DelivererOrdersState();
}

class _DelivererOrdersState extends State<DelivererOrders> with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController();

  List<Widget> _list = [
    SliderBox(child: AwaitingDeliveries()),
    SliderBox(child: CurrentDeliveries()),
  ];

  @override
  Widget build(BuildContext context) {
    PageIndicatorContainer container = new PageIndicatorContainer(
      child: new PageView(
        children: _list,
        controller: _pageController,
      ),
      length: _list.length,
      padding: EdgeInsets.fromLTRB(10, 40, 10, 10),
      indicatorSpace: 10,
      indicatorColor: Colors.grey,
      indicatorSelectorColor: Colors.grey,
    );

    return Scaffold(
      appBar: appBar(),
      body: Stack(
        children: <Widget>[
          Container(color: Colors.grey[100], height: double.infinity),
          Container(color: Colors.white, child: container, margin: EdgeInsets.only(bottom: 50)),
        ],
      ),
      bottomNavigationBar: AppBottomNavigationBar(currentIndex: BottomNavigationBarRoutes.HOME)
    );
  }
}