import 'package:flutter/material.dart';

class SliderBox extends StatelessWidget {
  final Widget child;

  SliderBox({ Key? key, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: child
    );
  }
}