import 'package:flutter/material.dart';
import 'package:goodfood/src/services/delivery/delivery.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/views/deliverer/delivery/delivery_page.dart';

class DeliveryBuilder extends StatelessWidget {
  final _deliveryService = new DeliveryService();
  final _storage = new Storage();
  final String orderId;

  DeliveryBuilder({ required this.orderId });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        _deliveryService.getDelivery(orderId),
        _storage.getUserId
      ]),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
        }
        return snapshot.hasData
            ? DeliveryPage(
                delivery: snapshot.data[0],
                userId: snapshot.data[1],
            )
            : Center(child: CircularProgressIndicator());
      },
    );
  }
}