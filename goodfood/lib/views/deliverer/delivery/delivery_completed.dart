import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/views/deliverer/delivery/image_preview.dart';
import 'package:goodfood/views/widgets/bottom_navigation_bar/deliverer/app_bottom_navigation_bar.dart';
import 'package:goodfood/views/widgets/bottom_navigation_bar/deliverer/bottom_navigation_bar_routes.dart';
import 'package:image_picker/image_picker.dart';

class DeliveryCompleted extends StatefulWidget {
  final String deliveryId;

  DeliveryCompleted({ required this.deliveryId });

  @override
  _DeliveryCompletedState createState() => _DeliveryCompletedState();
}

class _DeliveryCompletedState extends State<DeliveryCompleted> {
  XFile? _imageFile;
  dynamic _pickImageError;
  bool isVideo = false;
  String? _retrieveDataError;

  final ImagePicker _picker = ImagePicker();

  void _onImageButtonPressed(ImageSource source, {BuildContext? context}) async {
    try {
      final pickedFile = await _picker.pickImage(
        source: source,
        maxWidth: 1000,
        maxHeight: 1000,
        imageQuality: 90,
      );
      setState(() {
        if (pickedFile != null) {
          _imageFile = pickedFile;
          Navigator.push(context!, MaterialPageRoute(
              builder: (context) => ImagePreview(image: _imageFile!.path, deliveryId: widget.deliveryId)));
        }
      });
    } catch (e) {
      setState(() {
        _pickImageError = e;
      });
    }
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await _picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      isVideo = false;
      setState(() {
        _imageFile = response.file;
      });
    } else {
      _retrieveDataError = response.exception!.code;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: CircleAvatar(
                radius: 100.0,
                backgroundColor: Colors.grey,
                child: Icon(Icons.camera_alt_outlined, size: 86.0, color: Colors.white)),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.all(12),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          side: BorderSide(
                              color: Color(0xff0061737)
                          ),
                        ),
                        backgroundColor: Color(0xff0061737)
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(right: 10.0), child: Icon(Icons.camera_alt_outlined)),
                          Text("Caméra",
                            style: TextStyle(
                                fontSize: 16.0
                            ),
                          ),
                        ],
                      )
                    ),
                    onPressed: () {
                      _onImageButtonPressed(ImageSource.camera, context: context);
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10.0),
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.all(12),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          side: BorderSide(
                              color: Colors.transparent
                          ),
                        ),
                        backgroundColor: Colors.transparent
                    ),
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(right: 10.0), child: Icon(Icons.photo_library_outlined, color: Color(0xff0061737))),
                            Text("Galerie",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Color(0xff0061737)
                              ),
                            ),
                          ],
                        )
                    ),
                    onPressed: () {
                      _onImageButtonPressed(ImageSource.gallery, context: context);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: AppBottomNavigationBar(currentIndex: BottomNavigationBarRoutes.HOME),
    );
  }
}

typedef void OnPickImageCallback(double? maxWidth, double? maxHeight, int? quality);