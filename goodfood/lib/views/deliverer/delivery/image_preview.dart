import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/services/delivery/delivery.dart';
import 'package:goodfood/views/deliverer/orders/deliverer_orders.dart';
import 'package:goodfood/views/widgets/bottom_navigation_bar/deliverer/app_bottom_navigation_bar.dart';
import 'package:goodfood/views/widgets/bottom_navigation_bar/deliverer/bottom_navigation_bar_routes.dart';

class ImagePreview extends StatefulWidget {
  final String image;
  final String deliveryId;

  ImagePreview({ required this.image, required this.deliveryId });

  @override
  _ImagePreviewState createState() => _ImagePreviewState();
}

class _ImagePreviewState extends State<ImagePreview> {
  final _deliveryService = new DeliveryService();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: !isLoading
        ? Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: CircleAvatar(
              radius: 100.0,
              backgroundImage: Image.file(File(widget.image)).image,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.all(12),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          side: BorderSide(
                              color: Colors.green.shade600
                          ),
                        ),
                        backgroundColor: Colors.green.shade600
                    ),
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(right: 10.0), child: Icon(Icons.done)),
                            Text("Soumettre",
                              style: TextStyle(
                                  fontSize: 16.0
                              ),
                            ),
                          ],
                        )
                    ),
                    onPressed: () {
                      setState(() {
                        isLoading = true;
                      });
                      _deliveryService.deliveryCompleted(widget.deliveryId, File(widget.image)).then((response) {
                        setState(() {
                          isLoading = false;
                        });
                        print(widget.deliveryId);
                        if (response.statusCode == 200) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text("La livraison a bien été complétée !"),
                                backgroundColor: Colors.green,
                              )
                          );
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => DelivererOrders())
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text("Une erreur est survenue"),
                                backgroundColor: Colors.red,
                              )
                          );
                        }
                      });
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10.0),
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.all(12),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          side: BorderSide(
                              color: Colors.transparent
                          ),
                        ),
                        backgroundColor: Colors.transparent
                    ),
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(right: 10.0), child: Icon(Icons.cancel, color: Colors.red)),
                            Text("Annuler",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.red
                              ),
                            ),
                          ],
                        )
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ),
              ],
            ),
          ),
        ],
      ) : Center(child: CircularProgressIndicator()),
      bottomNavigationBar: AppBottomNavigationBar(currentIndex: BottomNavigationBarRoutes.HOME),
    );
  }
}