import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/delivery.dart';
import 'package:goodfood/src/services/delivery/delivery.dart';
import 'package:goodfood/src/services/user/user.dart';
import 'package:goodfood/views/deliverer/orders/map/awaiting/awaiting_delivery_map_builder.dart';
import '../orders/map/current/current_delivery_map_builder.dart';
import 'package:location/location.dart';

class DeliveryPage extends StatefulWidget {
  final Delivery delivery;
  final String userId;

  DeliveryPage({ required this.delivery, required this.userId });

  _DeliveryPageState createState() => _DeliveryPageState();
}

class _DeliveryPageState extends State<DeliveryPage> {
  final _deliveryService = new DeliveryService();
  final _userService = new UserService();
  String address = '';

  Location location = new Location();
  LocationData? _currentPosition;

  @override
  void initState() {
    super.initState();
    _userService.findAddresses(widget.userId).then((addresses) {
      Address firstAddress = addresses.first;
      setState(() {
        address = firstAddress.lane + ' ' + firstAddress.zipCode + ' ' + firstAddress.city;
      });
    });
  }

  _activateLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
  }
  /*_fetchLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _currentPosition = await location.getLocation();
    location.onLocationChanged.listen((LocationData currentLocation) {
      if (mounted) {
        setState(() {
          _currentPosition = currentLocation;
        });
      }
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: Text("Commande #${widget.delivery.id.substring(0, 7).toUpperCase()}",
          style: TextStyle(
              color: Color(0xff061737),
              fontSize: 14.0
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.3,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/unsplash_awj7sRviVXo.png'),
                  fit: BoxFit.fitHeight
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Text("The Good Food - Rive Droite",
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 32.0
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Text("$address",
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.grey.shade600
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 10, bottom: 10), child: Icon(Icons.person, size: 36.0)),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("Votre livraison par John Doe",
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(color: Colors.black, thickness: 1.0)
                ],
              )
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(right: 10, bottom: 10), child: Icon(Icons.shopping_bag, size: 36.0)),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text("Commande",
                        style: TextStyle(
                            fontSize: 16
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, bottom: 10),
                      child: Text("#${widget.delivery.id.substring(0, 7).toUpperCase()}",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    )
                  ],
                ),
                Divider(color: Colors.black, thickness: 1.0)
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, left: 100, right: 100, bottom: 50),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(top: 12, bottom: 12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    side: BorderSide(
                        color: Color(0xff061737)
                    ),
                  ),
                  backgroundColor: Color(0xff061737)
              ),
              child: Text("Commencer la livraison",
                style: TextStyle(
                    fontWeight: FontWeight.bold
                ),
              ),
              onPressed: () {
                _activateLocation();
                location.getLocation().then((location) {
                  print(location);
                });
                //_fetchLocation();
                location.getLocation().then((LocationData currentLocation) {
                  _deliveryService.createDelivery(
                      widget.delivery.id, currentLocation.latitude, currentLocation.longitude).then((response) {
                        print(response.statusCode);
                    if (response.statusCode == 201) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              AwaitingDeliveryMapBuilder(
                                delivery: widget.delivery,
                                deliveryId: jsonDecode(response.body)['id'],
                              ),
                          )
                      );
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text("Une erreur est survenue"),
                            backgroundColor: Colors.red,
                          )
                      );
                    }
                  });
                });
              }
            ),
          )
        ],
      ),
    );
  }
}