import 'package:flutter/material.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/views/login/login.dart';
import 'package:goodfood/views/registration/registration.dart';

class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(40),
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.3,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/logo/goodfood-icon.png'),
                  fit: BoxFit.fitHeight
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(40),
            child: Flexible(
              child: Text("Bienvenue sur ${Environment.get("APP_NAME")} notre plateforme de e-commerce où vous pouvez réaliser votre commande des produits que nous disposons et être livré chez vous en quelques minutes.",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 18.0,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 40, left: 40, right: 40, bottom: 10),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(top: 12, bottom: 12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(
                      color: Colors.grey.shade200,
                    ),
                  ),
                  primary: Colors.grey.shade200
              ),
              child: Text("Inscription",
                style: TextStyle(
                    color: Color(0xff061737),
                    fontWeight: FontWeight.bold
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RegistrationPage()));
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 40, left: 40, right: 40),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(top: 12, bottom: 12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    side: BorderSide(
                        color: Color(0xff061737)
                    ),
                  ),
                  primary: Color(0xff061737)
              ),
              child: Text("Connexion",
                style: TextStyle(
                  fontWeight: FontWeight.bold
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
            ),
          ),
        ],
      ),
    );
  }
}