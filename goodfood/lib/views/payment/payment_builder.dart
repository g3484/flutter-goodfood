import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/services/city/city.dart';
import 'package:goodfood/src/services/payment/payment.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/src/services/user/user.dart';
import 'package:goodfood/views/payment/payment.dart';

class PaymentBuilder extends StatefulWidget {
  final double price;

  PaymentBuilder({ required this.price });

  @override
  _PaymentBuilderState createState() => _PaymentBuilderState();
}

class _PaymentBuilderState extends State<PaymentBuilder> {
  final CityService _cityService = new CityService();
  final PaymentService _paymentService = new PaymentService();
  final Storage _storage = new Storage();
  final UserService _userService = new UserService();
  String? userId;

  @override
  void initState() {
    super.initState();
    _storage.getUserId.then((id) {
      setState(() {
        userId = id;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        _cityService.getCoordinates(),
        _paymentService.getPaymentOptions(),
        _userService.findAddresses(userId!)
      ]),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        return snapshot.hasData
            ? PaymentPage(
                coordinates: snapshot.data[0],
                paymentOptions: snapshot.data[1],
                addresses: snapshot.data[2],
                price: widget.price,
              )
            : Center(child: CircularProgressIndicator());
      },
    );
  }
}