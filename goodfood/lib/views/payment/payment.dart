import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/coordinates.dart';
import 'package:goodfood/src/models/payment_option.dart';
import 'package:goodfood/views/waiting/waiting_map.dart';
import '../waiting/waiting.dart';
import 'package:latlong2/latlong.dart';

class PaymentPage extends StatefulWidget {
  final Coordinates coordinates;
  final List<Address> addresses;
  final List<PaymentOption> paymentOptions;
  final double price;

  PaymentPage({
    required this.coordinates,
    required this.addresses,
    required this.paymentOptions,
    required this.price
  });

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  int itemIndex = 0;
  String? paymentOption;

  @override
  void initState() {
    super.initState();
    paymentOption = "Payer";
  }

  @override
  Widget build(BuildContext context) {
    Address address = widget.addresses.firstWhere((x) => x.mainAddress == true);

    return Scaffold(
      appBar: appBar(),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20),
            child: Text("Adresse",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 26.0,
                color: Color(0xff0061737)
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                          bottomRight: Radius.circular(30),
                          bottomLeft: Radius.circular(30),
                        ),
                        child: FlutterMap(
                          options: MapOptions(
                            center: LatLng(widget.coordinates.latitude, widget.coordinates.longitude),
                            zoom: 14.0,
                          ),
                          layers: [
                            TileLayerOptions(
                              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                              subdomains: ['a', 'b', 'c'],
                            ),
                            MarkerLayerOptions(
                              markers: <Marker>[
                                Marker(
                                  width: 80.0,
                                  height: 80.0,
                                  point: LatLng(widget.coordinates.latitude, widget.coordinates.longitude),
                                  builder: (ctx) =>
                                      Container(
                                        child: Icon(Icons.location_on, size: 40.0, color: Colors.blueAccent),
                                      ),
                                )
                              ]
                            )
                          ],
                        ),
                      )
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Text("Domicile",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0,
                            color: Color(0xff0061737),
                          ),
                        ),
                      ),
                      Text("${address.lane}",
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey
                        ),
                      ),
                      Text("${address.zipCode} ${address.city}",
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey
                        ),
                      ),
                    ],
                  )
                ],
              )
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("Méthode de paiement",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 26.0,
                  color: Color(0xff0061737)
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.15,
            child: Expanded(
              flex: 1,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: widget.paymentOptions.length,
                itemBuilder: (context, index) {
                  final item = widget.paymentOptions[index];
                  return Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(12),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                                side: BorderSide(
                                  color: itemIndex == item.id
                                    ? Color(0xff0061737)
                                    : Colors.grey,
                                  width: itemIndex == item.id ? 3 : 1,
                                ),
                              ),
                              backgroundColor: Colors.white
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.2,
                                height: MediaQuery.of(context).size.height * 0.05,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('${item.image}'),
                                      fit: BoxFit.fitWidth
                                  ),
                                ),
                              ),
                            ],
                          ),
                          onPressed: () => setState(() {
                            itemIndex = item.id;
                            paymentOption = "Continuer avec ${item.name}";
                          }),
                        ),
                      ),
                    ],
                  );
                }
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: DottedBorder(
              borderType: BorderType.RRect,
              radius: Radius.circular(12.0),
              strokeWidth: 2,
              color: Colors.green.shade700,
              dashPattern: [6, 6],
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(20),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      backgroundColor: Colors.green.shade100
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(Icons.discount_rounded, color: Colors.green.shade700),
                      Text("Ajouter un code discount",
                        style: TextStyle(
                            color: Colors.green.shade700,
                            fontSize: 18.0
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {},
                ),
              )
            )
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Total",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff0061737)
                  ),
                ),
                Text("${widget.price} €",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff0061737)
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(50, 20, 50, 20),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    side: BorderSide(
                        color: Color(0xff0061737)
                    ),
                  ),
                  backgroundColor: Color(0xff0061737)
              ),
              child: Text("$paymentOption",
                style: TextStyle(
                    fontSize: 16.0
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => WaitingMap())
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}