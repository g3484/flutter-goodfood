import 'package:flutter/material.dart';
import 'package:goodfood/config/environment.dart';
import 'package:goodfood/src/services/auth/auth.dart';
import 'package:goodfood/views/dashboard/dashboard_builder.dart';
import 'package:goodfood/views/home/homepage.dart';
import 'package:goodfood/views/orders/last_orders/last_orders_builder.dart';
import 'package:goodfood/views/orders/shopping_basket/shopping_basket_builder.dart';
import 'bottom_navigation_bar_routes.dart';

import 'bottom_navigation_bar.dart' as Navigation;

class AppBottomNavigationBar extends StatelessWidget {
  final _auth = new AuthService();
  final int currentIndex;

  AppBottomNavigationBar({ required this.currentIndex });

  @override
  Widget build(BuildContext context) {
    return Navigation.BottomNavigationBar(
      currentIndex: currentIndex,
      centerButtonIcon: Icon(Icons.search),
      centerButtonOnPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Homepage()));
      },
      centerButtonColor: Environment.getColorTheme(),
      color: Colors.grey,
      backgroundColor: Colors.white,
      selectedColor: Colors.black,
      notchedShape: CircularNotchedRectangle(),
      onTabSelected: (int id) {
        switch (id) {
          case BottomNavigationBarRoutes.HOME:
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Dashboard()));
            break;
          case BottomNavigationBarRoutes.LAST_ORDERS:
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => LastOrdersBuilder()));
            break;
          case BottomNavigationBarRoutes.SHOPPING_BASKET:
            Navigator.push(context, MaterialPageRoute(
                builder: (context) => ShoppingBasketBuilder()));
            break;
          case BottomNavigationBarRoutes.SETTINGS:
            _buildDialog(context);
            break;
        }
      },
      items: [
        Navigation.BottomNavigationBarItem(icon: Icon(Icons.home)),
        Navigation.BottomNavigationBarItem(icon: Icon(Icons.summarize_outlined)),
        Navigation.BottomNavigationBarItem(icon: Icon(Icons.shopping_cart)),
        Navigation.BottomNavigationBarItem(icon: Icon(Icons.settings)),
      ],
    );
  }

  Widget bottomDropdownMenu(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          color: Colors.white,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Homepage()));
                },
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 10.0),
                          child: Icon(Icons.account_circle_rounded,
                              color: Colors.grey)),
                      Text("Mon profil", style: TextStyle(fontSize: 18.0))
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Homepage()));
                },
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 10.0),
                          child: Icon(Icons.settings, color: Colors.grey)),
                      Text("Paramètres", style: TextStyle(fontSize: 18.0))
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _auth.logout(context);
                },
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 10.0),
                          child: Icon(Icons.logout, color: Colors.grey)),
                      Text("Se déconnecter", style: TextStyle(fontSize: 18.0))
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  void _buildDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return bottomDropdownMenu(context);
        });
  }
}