import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BottomNavigationBarItem {
  Widget icon;
  String? text;

  BottomNavigationBarItem({ required this.icon, this.text });
}

class BottomNavigationBar extends StatefulWidget {
  final List<BottomNavigationBarItem> items;
  final Icon centerButtonIcon;
  final VoidCallback? centerButtonOnPressed;
  final Color centerButtonColor;
  final double height;
  final double iconSize;
  final Color backgroundColor;
  final Color color;
  final Color selectedColor;
  final NotchedShape notchedShape;
  final ValueChanged<int> onTabSelected;
  int currentIndex;

  BottomNavigationBar({
    required this.items,
    required this.centerButtonIcon,
    required this.centerButtonOnPressed,
    required this.centerButtonColor,
    this.height: 60.0,
    this.iconSize: 24.0,
    required this.backgroundColor,
    required this.color,
    required this.selectedColor,
    required this.notchedShape,
    required this.onTabSelected,
    required this.currentIndex
  }) {
    assert(this.items.length == 2 || this.items.length == 4);
  }

  @override
  _BottomNavigationBarState createState() => _BottomNavigationBarState();
}

class _BottomNavigationBarState extends State<BottomNavigationBar> {
  _updateIndex(int index) {
    widget.onTabSelected(index);
    setState(() {
      widget.currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = List.generate(widget.items.length, (int index) {
      return _buildTabItem(
        item: widget.items[index],
        index: index,
        onPressed: _updateIndex,
      );
    });
    items.insert(items.length >> 1, _buildMiddleTabItem());

    return BottomAppBar(
      shape: widget.notchedShape,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: items,
      ),
      color: widget.backgroundColor,
    );
  }

  Widget _buildMiddleTabItem() {
    return Expanded(
      child: SizedBox(
        height: widget.height,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FloatingActionButton(
              onPressed: widget.centerButtonOnPressed,
              child: widget.centerButtonIcon,
              elevation: 2.0,
              backgroundColor: widget.centerButtonColor,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTabItem({
    required BottomNavigationBarItem item,
    required int index,
    required ValueChanged<int> onPressed,
  }) {
    Color color = widget.currentIndex == index ? widget.selectedColor : widget.color;
    return Expanded(
      child: SizedBox(
        height: widget.height,
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            onTap: () => onPressed(index),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconTheme(
                  data: IconThemeData(color: color),
                  child: item.icon,
                ),
                if (item.text != null)
                  Text(
                    item.text!,
                    style: TextStyle(color: color),
                  )
              ],
            ),
          ),
        ),
      ),
    );
  }
}