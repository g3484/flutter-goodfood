class BottomNavigationBarRoutes {
  static const int HOME = 0;
  static const int LAST_ORDERS = 1;
  static const int SHOPPING_BASKET = 2;
  static const int SETTINGS = 3;
}