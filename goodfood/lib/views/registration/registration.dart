import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:goodfood/config/environment.dart';
import '../../src/forms/registration/registration_form.dart';

class RegistrationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Environment.getColorTheme()),
        elevation: 0.0,
      ),
      body: RegistrationForm(),
    );
  }
}