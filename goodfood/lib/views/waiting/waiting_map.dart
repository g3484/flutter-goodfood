import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import '../widgets/bottom_navigation_bar/user/app_bottom_navigation_bar.dart';
import 'package:latlong2/latlong.dart';

class WaitingMap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("5min - 3km",
              style: TextStyle(
                  color: Color(0xff0061737),
                  fontWeight: FontWeight.bold
              ),
            ),
            Text("Livraison par Bob",
              style: TextStyle(
                  color: Colors.grey.shade700,
                  fontSize: 14
              ),
            )
          ],
        ),
      ),
      body: FlutterMap(
        options: MapOptions(
          center: LatLng(49.443639, 1.10344),
          zoom: 17.0,
        ),
        layers: [
          TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c'],
          ),
          MarkerLayerOptions(
              markers: <Marker>[
                Marker(
                  width: 80.0,
                  height: 80.0,
                  point: LatLng(49.443639, 1.10344),
                  builder: (ctx) =>
                      Container(
                        child: Icon(Icons.location_on, size: 40.0, color: Colors.blueAccent),
                      ),
                )
              ]
          )
        ],
      ),
      bottomNavigationBar: AppBottomNavigationBar(currentIndex: 1),
    );
  }
}