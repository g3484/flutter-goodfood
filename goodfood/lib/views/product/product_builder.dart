import 'package:flutter/material.dart';
import 'package:goodfood/src/services/product/product.dart';
import 'package:goodfood/views/product/product_page.dart';

class ProductBuilder extends StatelessWidget {
  final String productId;
  final _productService = new ProductService();

  ProductBuilder({ required this.productId });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _productService.getProduct(productId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
        }
        return snapshot.hasData
            ? ProductPage(product: snapshot.data)
            : Center(child: CircularProgressIndicator());
      }
    );
  }
}