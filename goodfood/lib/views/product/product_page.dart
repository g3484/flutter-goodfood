import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/src/services/product/product.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/src/services/user/user.dart';
import '../widgets/bottom_navigation_bar/user/app_bottom_navigation_bar.dart';
import '../widgets/bottom_navigation_bar/user/bottom_navigation_bar_routes.dart';

class ProductPage extends StatefulWidget {
  final Product product;

  ProductPage({ required this.product });

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final _productService = new ProductService();
  final _storage = new Storage();
  final _userService = new UserService();

  String mailAddress = "";
  String address = "";

  @override
  void initState() {
    super.initState();
    _storage.getUsername.then((username) {
      setState(() {
        mailAddress = username;
      });
    });
    _storage.getUserId.then((id) {
      _userService.findAddresses(id).then((addresses) {
        Address addressToString = addresses.first;
        setState(() {
          address = addressToString.lane + ' ' + addressToString.zipCode + ' ' + addressToString.city;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30),
              ),
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.3,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: Image.network(widget.product.imageUrl).image,
                      fit: BoxFit.cover
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("${widget.product.name}",
              style: TextStyle(
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
                color: Color(0xff061737)
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text("${widget.product.description}",
              style: TextStyle(
                fontSize: 16.0
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Card(
                    color: Colors.grey.shade300,
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(Icons.euro, color: Colors.grey.shade700),
                          Text("${widget.product.price}",
                            style: TextStyle(
                                color: Colors.grey.shade700,
                                fontSize: 16.0
                            ),
                          ),
                        ],
                      ),
                    )
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.all(12),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          side: BorderSide(
                              color: Color(0xff0061737)
                          ),
                        ),
                        backgroundColor: Color(0xff0061737)
                    ),
                    child: Text("Ajouter au panier",
                      style: TextStyle(
                          fontSize: 16.0
                      ),
                    ),
                    onPressed: () {
                      List<Product> products = [];
                      products.add(widget.product);
                      _productService.addToCart(products);
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text("Cet article a été ajouté à votre panier !"),
                            backgroundColor: Colors.green,
                          )
                      );
                    },
                  ),
                ),
              ],
            )
          ),
        ],
      ),
      bottomNavigationBar: AppBottomNavigationBar(currentIndex: BottomNavigationBarRoutes.HOME),
    );
  }
}