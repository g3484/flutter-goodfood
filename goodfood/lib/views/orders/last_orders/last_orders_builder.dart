import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/services/order/order.dart';
import 'package:goodfood/views/orders/last_orders/last_orders.dart';

class LastOrdersBuilder extends StatelessWidget {
  final _orderService = new OrderService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _orderService.getOrders(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasError) {
          print("Error : ${snapshot.stackTrace}");
        }
        return snapshot.hasData
            ? LastOrders(orders: snapshot.data)
            : Scaffold(body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}