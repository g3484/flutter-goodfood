import 'package:flutter/material.dart';
import 'package:goodfood/src/extensions/string_extension.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/order.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/src/services/date/date_converter.dart';
import 'package:goodfood/src/services/order/order_status.dart';
import 'package:goodfood/views/orders/no_order_found.dart';
import 'package:goodfood/views/orders/order_details.dart';
import '../../widgets/bottom_navigation_bar/user/app_bottom_navigation_bar.dart';
import '../../widgets/bottom_navigation_bar/user/bottom_navigation_bar_routes.dart';

class LastOrders extends StatelessWidget {
  final List<Order> orders;

  LastOrders({ required this.orders });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: appBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Text("Mes commandes",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 28.0,
                color: Color(0xff061737),
              ),
            ),
          ),
          Flexible(
            child: this.orders.length > 0 
              ? ListView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                itemCount: this.orders.length,
                itemBuilder: (BuildContext context, int index) {
                  Order order = this.orders[index];
                  Product product = Product.fromJson(order.products.first);
                  return GestureDetector(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.15,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: Image.network("${product.imageUrl}").image,
                                  fit: BoxFit.fitHeight
                              ),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15.0),
                                  bottomLeft: Radius.circular(15.0)
                              ),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Text("${product.name}",
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 15.0, bottom: 15.0),
                                child: Text("${order.price} €",
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w400
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 15.0),
                                child: Text("Le ${DateConverter.toFrenchFormatFromUTCType(order.dateOfOrder)}",
                                  style: TextStyle(
                                      fontSize: 10.0,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff0B0B4BE)
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 15.0, bottom: 15.0),
                                child: Text("${OrderStatus.translateStatus(order.status.toUpperCase()).capitalize()}",
                                  style: TextStyle(
                                      fontSize: 10.0,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff0B0B4BE)
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              OrderDetails(
                                order: order,
                                product: product
                              )
                          )
                      );
                    },
                  );
                }
            ) : NoOrderFound(),
          ),
        ],
      ),
      bottomNavigationBar: AppBottomNavigationBar(currentIndex: BottomNavigationBarRoutes.LAST_ORDERS),
    );
  }
}