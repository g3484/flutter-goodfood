import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/src/models/user.dart';
import 'package:goodfood/src/services/order/order.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/src/services/user/user.dart';
import 'package:goodfood/views/orders/no_order_found.dart';
import 'package:goodfood/views/payment/payment_builder.dart';
import '../../widgets/bottom_navigation_bar/user/app_bottom_navigation_bar.dart';
import '../../widgets/bottom_navigation_bar/user/bottom_navigation_bar_routes.dart';

class ShoppingBasket extends StatefulWidget {
  final List<Product> products;

  ShoppingBasket({ required this.products });

  @override
  _ShoppingBasketState createState() => _ShoppingBasketState();
}

class _ShoppingBasketState extends State<ShoppingBasket> {
  final _orderService = new OrderService();
  final _userService = new UserService();
  final _storage = new Storage();
  int ordersNumber = 0;
  double totalPrice = 0.0;
  List<int> prices = [];

  String username = '';
  String address = '';

  @override
  void initState() {
    super.initState();
    _storage.getUserId.then((id) {
      _userService.findById(id).then((user) {
        username = user.mailAddress;
      });
      _userService.findAddresses(id).then((addresses) {
        Address firstAddress = addresses.first;
        address = firstAddress.lane + ' ' + firstAddress.zipCode + ' ' + firstAddress.city;
      });
    });
    setState(() {
      ordersNumber = widget.products.length;
    });
    for (int i = 0; i < ordersNumber; i++) {
      List<Product> items = widget.products;
      prices.add(int.parse(items[i].price));
      setState(() {
        totalPrice = prices.sum.toDouble();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: appBar(),
      body: ordersNumber > 0
        ? Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("${ordersNumber > 1 ? "$ordersNumber éléments" : "$ordersNumber élément"}",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(12),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                        side: BorderSide(
                            color: Color(0xff08190F5)
                        ),
                      ),
                      backgroundColor: Color(0xff08190F5)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Icon(Icons.add),
                      Text("Ajouter",
                        style: TextStyle(
                            fontSize: 16.0
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: widget.products.length,
                  itemBuilder: (BuildContext context, int index) {
                    Product product = widget.products[index];
                    return Dismissible(
                      key: Key(index.toString()),
                      confirmDismiss: (direction) {
                        return showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text('Suppression'),
                                content: Text('Êtes-vous sûr de vouloir supprimer cet élément ?'),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(false);
                                    },
                                    child: Text('Non'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      setState(() {
                                        ordersNumber--;
                                        totalPrice = totalPrice - int.parse(product.price);
                                      });
                                      ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Votre commande a bien été supprimée !"),
                                            backgroundColor: Colors.green,
                                          )
                                      );
                                      Navigator.of(context).pop(true);
                                    },
                                    child: Text('Oui'),
                                  ),
                                ],
                              );
                            });
                      },
                      direction: DismissDirection.endToStart,
                      background: Container(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 15.0),
                          child: Icon(
                            Icons.delete,
                            size: 40.0,
                            color: Colors.white,
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.red,
                        ),
                      ),
                      child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width * 0.3,
                                      height: MediaQuery
                                          .of(context)
                                          .size
                                          .height * 0.15,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: Image.network(product.imageUrl).image,
                                            fit: BoxFit.fitHeight
                                        ),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(15.0),
                                            bottomLeft: Radius.circular(15.0)
                                        ),
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.all(15.0),
                                          child: Text("${product.name}",
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.w500
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: 15.0, bottom: 15.0),
                                          child: Text("${product.price} €",
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.w500
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 10.0),
                                      child: GestureDetector(
                                        child: Icon(Icons.remove_circle_outline, color: Color(0xff0E5E5E5)),
                                        onTap: () {
                                          setState(() {
                                            if (product.quantity > 1) {
                                              product.quantity--;
                                              totalPrice = totalPrice - int.parse(product.price);
                                            }
                                          });
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 10.0),
                                      child: Text("${product.quantity}",
                                        style: TextStyle(
                                            fontSize: 18.0
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                        child: Icon(Icons.add_circle, color: Color(0xff0061737)),
                                        onTap: () {
                                          setState(() {
                                            product.quantity++;
                                            totalPrice = totalPrice + int.parse(product.price);
                                          });
                                        }
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                      ),
                    );
                  }
              ),
            )
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Total",
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Color(0xff0061737),
                  ),
                ),
                Text("$totalPrice €",
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Color(0xff0061737),
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            width: width * 0.7,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    side: BorderSide(
                        color: Color(0xff0061737)
                    ),
                  ),
                  backgroundColor: Color(0xff0061737)
              ),
              child: Text("Payer",
                style: TextStyle(
                    fontSize: 16.0
                ),
              ),
              onPressed: () {
                List<String> productsIds = [];
                widget.products.forEach((product) {
                  productsIds.add("${product.id}");
                });
                _orderService.addOrder(username, address, productsIds).then((response) {
                  if (response.statusCode == 201) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text("Cet article a été ajouté à votre panier !"),
                          backgroundColor: Colors.green,
                        )
                    );
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PaymentBuilder(price: totalPrice))
                    );
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text("Une erreur est survenue"),
                          backgroundColor: Colors.red,
                        )
                    );
                  }
                });
              },
            ),
          )
        ],
      ) : NoOrderFound(),
      bottomNavigationBar: AppBottomNavigationBar(
          currentIndex: BottomNavigationBarRoutes.SHOPPING_BASKET),
    );
  }
}