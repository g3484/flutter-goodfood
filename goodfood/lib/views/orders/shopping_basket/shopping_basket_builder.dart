import 'package:flutter/material.dart';
import 'package:goodfood/src/services/product/product.dart';
import 'package:goodfood/views/orders/shopping_basket/shopping_basket.dart';

class ShoppingBasketBuilder extends StatelessWidget {
  final _productService = new ProductService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _productService.getCartContent(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          print(snapshot.stackTrace);
        }
        return snapshot.hasData
            ? ShoppingBasket(products: snapshot.data)
            : Center(child: CircularProgressIndicator());
      },
    );
  }
}