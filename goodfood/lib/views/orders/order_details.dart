import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/order.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/src/services/date/date_converter.dart';

import '../../src/services/order/order_status.dart';

class OrderDetails extends StatelessWidget {
  final Order order;
  final Product product;

  OrderDetails({ required this.order, required this.product });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        title: Text("${product.name}",
          style: TextStyle(
              color: Color(0xff061737),
              fontSize: 14.0
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.3,
            decoration: BoxDecoration(
              image: DecorationImage(
               image: AssetImage('assets/images/unsplash_awj7sRviVXo.png'),
               fit: BoxFit.fitHeight
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Text("The Good Food - Rive Droite",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 32.0
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 40.0, top: 10.0),
            child: Row(
              children: <Widget>[
                Text("Commande ${OrderStatus.translateStatus(order.status.toUpperCase())}",
                  style: TextStyle(
                    color: Color(0xff08190F5),
                    fontSize: 14.0
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Icon(Icons.circle, size: 12.0, color: Color(0xff0B0B4BE)),
                ),
                Text("Le ${DateConverter.toFrenchFormatFromUTCType(order.dateOfOrder)}",
                  style: TextStyle(
                    color: Color(0xff0B0B4BE),
                    fontSize: 14.0
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 40.0, left: 20.0),
            child: Text("Votre commande",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Row(
              children: <Widget>[
                Container(
                  width: 50,
                  height: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey.shade300
                  ),
                  child: Text("1",
                    style: TextStyle(
                      fontSize: 22
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text("${product.name}",
                    style: TextStyle(
                      fontSize: 18.0
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 10, bottom: 10), child: Icon(Icons.person, size: 36.0)),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("Votre livraison par John Doe",
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(color: Colors.black, thickness: 1.0)
                ],
              )
          ),
          Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(right: 10, bottom: 10), child: Icon(Icons.person, size: 36.0)),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text("Total : ${order.price} €",
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(color: Colors.black, thickness: 1.0)
                ],
              ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, left: 100, right: 100, bottom: 50),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(top: 12, bottom: 12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    side: BorderSide(
                        color: Color(0xff061737)
                    ),
                  ),
                  primary: Color(0xff061737)
              ),
              child: Text("Voir le reçu",
                style: TextStyle(
                    fontWeight: FontWeight.bold
                ),
              ),
              onPressed: () {
              },
            ),
          )
        ],
      ),
    );
  }
}