import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/services/auth/auth.dart';
import 'package:goodfood/src/services/jwt/jwt.dart';
import 'package:goodfood/src/services/product/product.dart';
import 'package:goodfood/src/services/storage/storage.dart';
import 'package:goodfood/src/services/user/user.dart';
import 'package:goodfood/views/dashboard/dashboard_page.dart';
import 'package:jwt_decode/jwt_decode.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final _auth = new AuthService();
  final _storage = new Storage();
  final _productService = new ProductService();
  final _userService = new UserService();
  final jwtService = new JwtService();
  String userId = '';

  @override
  void initState() {
    super.initState();
    _storage.getUserId.then((id) {
      setState(() {
        userId = id;
      });
    });
  }

  @override
  void dispose() {
    _storage.getUserId;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.wait([
          _productService.getCategories(),
          _productService.getProducts(),
          _userService.findById(userId),
          _userService.findAddresses(userId),
          _storage.getJwt
        ]),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            print("Error : ${snapshot.stackTrace}");
          }
          return snapshot.hasData
              ? DashboardPage(
                  categories: snapshot.data[0],
                  products: snapshot.data[1],
                  user: snapshot.data[2],
                  addresses: snapshot.data[3])
              : Scaffold(
                  body: Center(child: CircularProgressIndicator()),
                );
        }
    );
  }
}