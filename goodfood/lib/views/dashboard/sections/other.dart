import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/views/dashboard/widgets/section_title.dart';
import 'package:goodfood/views/product/product_builder.dart';
import 'package:goodfood/views/product/product_page.dart';

class Other extends StatelessWidget {
  final List<Product> products;

  Other({ required this.products });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SectionTitle(title: "Autres"),
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2),
            itemCount: this.products.length,
            itemBuilder: (BuildContext context, int index) {
              Product product = this.products[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ProductBuilder(productId: product.id))
                  );
                },
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Card(
                      child: Column(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30),
                              bottomRight: Radius.circular(30),
                              bottomLeft: Radius.circular(30),
                            ),
                            child: Container(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width * 0.2,
                              height: MediaQuery
                                  .of(context)
                                  .size
                                  .height * 0.1,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: Image.network(product.imageUrl).image,
                                    fit: BoxFit.fitHeight
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text("${product.name}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      overflow: TextOverflow.ellipsis,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0
                                  ),
                                ),
                              )
                          ),
                          Flexible(
                              child: Padding(
                                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                                child: Text("${product.description}",
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color(0xff0616161),
                                      fontWeight: FontWeight.w400
                                  ),
                                ),
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.all(15.0),
                            child: Text("${product.price} €",
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                ),
              );
            }
          ),
        )
      ],
    );
  }
}