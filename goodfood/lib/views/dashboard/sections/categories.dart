import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/models/category.dart';

class Categories extends StatelessWidget {
  final List<Category> categories;

  Categories({ required this.categories });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.1,
      child: Expanded(
        flex: 1,
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: this.categories.length,
            itemBuilder: (BuildContext context, int index) {
              final item = this.categories[index];
              return Padding(
                padding: EdgeInsets.all(10.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(12),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                        side: BorderSide(
                            color: Color(0xff08190F5)
                        ),
                      ),
                      backgroundColor: Color(0xff08190F5)
                  ),
                  child: Text("${item.name}",
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  onPressed: () {},
                ),
              );
            }),
      ),
    );
  }
}