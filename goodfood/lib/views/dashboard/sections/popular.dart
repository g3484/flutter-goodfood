import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/views/dashboard/widgets/section_title.dart';
import 'package:goodfood/views/product/product_builder.dart';
import 'package:goodfood/views/product/product_page.dart';

class Popular extends StatelessWidget {
  final List<Product> products;

  Popular({ required this.products });

  @override
  Widget build(BuildContext context) {
    List<Product> popular = this.products..shuffle();
    return Column(
      children: <Widget>[
        SectionTitle(title: "Populaires"),
        Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: MediaQuery
              .of(context)
              .size
              .height * 0.5,
          child: Expanded(
            flex: 1,
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 15,
                itemBuilder: (BuildContext context, int index) {
                  Product product = popular[index];
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ProductBuilder(productId: product.id))
                      );
                    },
                    child: Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30),
                              bottomRight: Radius.circular(30),
                              bottomLeft: Radius.circular(30),
                            ),
                            child: Container(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,
                              height: MediaQuery
                                  .of(context)
                                  .size
                                  .height * 0.3,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: Image.network(product.imageUrl).image,
                                    fit: BoxFit.cover
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("${product.name}",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Card(
                                        color: Colors.grey.shade300,
                                        child: Padding(
                                          padding: EdgeInsets.all(15),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Icon(Icons.euro, color: Colors.grey.shade700),
                                              Text("${product.price}",
                                                style: TextStyle(
                                                    color: Colors.grey.shade700,
                                                    fontSize: 16.0
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ),
                                    Card(
                                        color: Colors.grey.shade300,
                                        child: Padding(
                                          padding: EdgeInsets.all(15),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Icon(Icons.star, color: Colors.grey.shade700),
                                              Text("4.5",
                                                style: TextStyle(
                                                    color: Colors.grey.shade700,
                                                    fontSize: 16.0
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}