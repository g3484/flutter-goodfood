import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodfood/src/helpers/helpers.dart';
import 'package:goodfood/src/models/address.dart';
import 'package:goodfood/src/models/category.dart';
import 'package:goodfood/src/models/product.dart';
import 'package:goodfood/src/models/user.dart';
import 'package:goodfood/views/dashboard/sections/categories.dart';
import 'package:goodfood/views/dashboard/sections/new_on_app.dart';
import 'package:goodfood/views/dashboard/sections/other.dart';
import 'package:goodfood/views/dashboard/sections/popular.dart';
import '../widgets/bottom_navigation_bar/user/app_bottom_navigation_bar.dart';
import '../widgets/bottom_navigation_bar/user/bottom_navigation_bar_routes.dart';

class DashboardPage extends StatelessWidget {
  final List<Category> categories;
  final List<Product> products;
  final User user;
  final List<Address> addresses;

  DashboardPage({
    required this.categories,
    required this.products,
    required this.user,
    required this.addresses
  });

  @override
  Widget build(BuildContext context) {
    Address address = addresses.firstWhere((x) => x.mainAddress == true);

    return Scaffold(
      appBar: appBar(
          title: Row(
            children: <Widget>[
              Icon(Icons.location_on, color: Color(0xff061737)),
              Text("${address.lane} ${address.zipCode} ${address.city}",
                style: TextStyle(
                    color: Color(0xff061737),
                    fontSize: 14.0
                ),
              )
            ],
          )
      ),
      body: ListView(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          children: <Widget>[
            NewOnApp(),
            Categories(categories: this.categories),
            Popular(products: this.products),
            Other(products: this.products),
          ]
      ),
      bottomNavigationBar: AppBottomNavigationBar(
          currentIndex: BottomNavigationBarRoutes.HOME),
    );
  }
}