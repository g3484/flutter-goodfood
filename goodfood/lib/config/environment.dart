import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

/// Classe Environment
/// Contient les méthodes relatives aux variables d'environnement
/// Ces variables sont contenues dans le fichier .env
class Environment {
  /// Récupération d'une variable d'environnement
  /// String [variable] correspond au nom de la variable d'environnement
  /// Returns variable d'environnement
  static String get(String variable) => dotenv.env[variable].toString();

  /// Récupération de la couleur du thème
  /// Returns couleur du thème
  static Color getColorTheme() => Color(0xffABADF4);
}